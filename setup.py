from setuptools import find_packages, setup



setup(name='testTools',
packages=find_packages(include=['testTools','testTools.*']),
version='5.9.00',
description='EAFS',
author='Gabriel Alin Munteanu',
license='',
install_requires=[
'selenium',
'requests',
'workalendar',
'json2html',
'pymsteams',
'pandas',
'xlsxwriter',
'xlrd',
'bcolors',
'pynput',
'halo'],
setup_requires=[]
)
