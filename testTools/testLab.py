from testTools.utilities import utilities as utils
import importlib
import re
import bcolors
from datetime import datetime as dt
from testTools.loggingMechanism import VDFLogging
from testTools.raportHTML import generateRaport
import os, __main__
from pathlib import Path
import inspect
import numpy as np


# def my_decorator(func):
#     def wrapper():
#         print("Something is happening before the function is called.")
#         func()
#         print("Something is happening after the function is called.")
#     return wrapper

# @my_decorator
# def trest():
#     print("test32141323")

class testLab:
    tracker = 1
    browser = None
    utils = utils()
    logging = VDFLogging()
    execution_Starter_Module = None
    pom_ConditionsList = None
    test_Name = ''

    def __init__(self, testDefinitionsDir, applicationName='Default', execModulesList=None, isDataPrepRequired=True,
                 logTestExceptions=True, testSuit=None, no_of_docker_instances=1):
        self.no_of_docker_instances = no_of_docker_instances
        self.utils.setTempValue("app", applicationName)
        self.createOutputStructure()
        self.logTestExceptions = logTestExceptions
        self.testDataDictionary = {}
        self.isDataPrepRequired = isDataPrepRequired
        self.execModulesList = execModulesList
        self.applicationName = applicationName
        self.testDefinitionsDir = testDefinitionsDir
        self.target_Test_DirPath = (
            self.utils.dir_list_folder(head_dir=os.path.dirname(inspect.getfile(self.__class__)),
                                       dir_name=self.testDefinitionsDir))[0]
        # self.testDefinitionsDir = self.testDirectoryPath()
        self.testSuit = testSuit
        self.testDefinitionsDir = testDefinitionsDir
        self.pom_ConditionsList = []
        # Child className (from the inheriting class @runtime)
        self.execution_Starter_Module = type(self).__module__

        # Usually suitrunner class, basically setsup browser/starts tests
        self.scanChildClassModule()

    def scanChildClassModule(self):
        suitRunner_test_Module = importlib.import_module(self.execution_Starter_Module)
        suitRunner_test_Class = getattr(suitRunner_test_Module, self.__class__.__name__)

        myTestSuit = self.get_myTestSuit()
        if myTestSuit != "Invalid testSuit":
            for testCase in myTestSuit:
                for testName, pom_ConditionsList in testCase.items():
                    self.test_Name = testName
                    self.pom_ConditionsList = pom_ConditionsList
                    if self.setUpTestData() != False:
                        getattr(suitRunner_test_Class, "setUp_Browser")(self)
                        getattr(suitRunner_test_Class, "setUp_dockerEnvironment")(self)
                        getattr(suitRunner_test_Class, "start_test_execution")(self)
                        getattr(suitRunner_test_Class, "closeBrowser")(self)
                    else:
                        print(f"Test not executed, data not received {self.test_Name}")

    def suitRunner(self, browser):
        self.browser = browser
        # print(self.test_Name + ' ' + self.tracker)
        execCount = 0
        loopCount = 0
        pom_Module_NamesLst = []
        testCompletionText = f'{bcolors.OK}--------------------------------------[Test Execution Completed]--------------------------------------\n{bcolors.END}'

        # self.testName_setter(testName)
        self.logging = VDFLogging(testCaseName=self.test_Name)
        print(f'{bcolors.BITALIC}{bcolors.ERR}{bcolors.BLUE}{self.test_Name} has started!{bcolors.END}')
        ##For every page name in the dictonary, execute test method
        if self.pom_ConditionsList is not None:

            for pom_Item in self.pom_ConditionsList:
                for key_as_page, testConditions in pom_Item.items():
                    pom_Module_NamesLst.append(list(pom_Item.keys())[0])
                    loopCount += 1
                    if self.execModulesList is None:
                        self.dynamicTestExec(key_as_page, self.target_Test_DirPath)
                    elif key_as_page in self.execModulesList:
                        self.dynamicTestExec(key_as_page, self.target_Test_DirPath)
                        execCount = 1
                    elif loopCount == len(self.pom_ConditionsList) and execCount == 0:
                        print(
                            f'{bcolors.ERR}{bcolors.BOLD}\t!~For specific modules list {self.execModulesList} no match found in {pom_Module_NamesLst}{bcolors.END}')

                    if self.handleMethod_Log():
                        print(
                            f'{bcolors.ERR}{bcolors.BOLD}\tTest execution stopped because {bcolors.ERRMSG}CRITICAL FAIL occured{bcolors.END}')
                        break
            print(testCompletionText)
            generateRaport(resultD=self.handleTestCase_Log(), testName=self.test_Name, value=self.pom_ConditionsList,
                           trustedList=pom_Module_NamesLst)
        else:
            print(
                f"{bcolors.ERR}{bcolors.ERRMSG} POM CONDITIONS ARE NONE, contact developement department! {bcolors.ENDC}")

    def dynamicTestExec(self, key_as_page, testConditions):

        ##Does module import based on string path starting from ./SRC...page_Name from dictionary
        # Exapmle SRC.test_Definitions.PI105.test_page_login
        if os.name == 'nt':
            target_test_Module = importlib.import_module(fr'{self.testDefinitionsDir}.{key_as_page}'.replace("\\", "."))
        else:
            target_test_Module = importlib.import_module(fr'{self.testDefinitionsDir}.{key_as_page}'.replace("/", "."))
            ##Gets class from above Module, based on key from dictionary stored in var key_as_page
        # Ex. key_as_page = page_login
        target_test_Class = getattr(target_test_Module, key_as_page)
        ##Creates an instance based on above class, initializating it's contructor with inputDictionary
        try:
            print(
                f'{bcolors.OK}* Test Data assigned to => {target_test_Class.__name__}\n\t*{self.testDataDictionary}{bcolors.ENDC}')
            page_Instance = target_test_Class(inputDictionary=self.pom_ConditionsList, browser=self.browser,
                                              testName=self.test_Name, testData=self.testDataDictionary)
        except ModuleNotFoundError:
            page_Instance = target_test_Class(inputDictionary=self.pom_ConditionsList, browser=self.browser,
                                              testName=self.test_Name)
            if self.logTestExceptions:
                self.format_stacktrace()

        ##Call Method that is named test_ + key_as_page value
        # Ex. test_page_login()
        self.itsMGA()
        print(f'{bcolors.WAITMSG}{bcolors.ERR}{bcolors.BLUE}TEST_{key_as_page.upper()} has started...{bcolors.END}')

        try:
            getattr(page_Instance, f"test_{key_as_page}")()
        except Exception as e:
            if self.logTestExceptions:
                self.format_stacktrace()

    def testDirectoryPath(self):
        ##Strips from dex till end of string
        # (scope is to get root dir for current module, should be "SRC")
        ##TODO  NEED 2 FGIX HARDCODED PATH!!!!
        sourceDirName = os.path.dirname(__main__.__file__).split("/")[-1]
        if os.name == 'nt':
            root_Dir = re.search(f'{sourceDirName}\\\(.*)', self.target_Test_DirPath).group(1)
        else:
            root_Dir = re.search(f'{sourceDirName}/(.*)', self.target_Test_DirPath).group(1)
            # return testDefinitionsDir
        return root_Dir

    def handleTestCase_Log(self):
        isCritical, testCase_Log = self.logging.analyseLog(log_dict=self.logging.testCase_log, log_type='TESTCASE',
                                                           host='MyHst', release_name="TBD",
                                                           exec_TS=dt.now().strftime('%Y%m%d%H%M%S'),
                                                           connector_log='NotGIven')
        return testCase_Log

    def handleMethod_Log(self):
        method_Log = self.utils.VDFloggingObj.testMethod_OutputLog
        isCritical, method_Log = self.logging.analyseLog(log_dict=method_Log, log_type='METHOD')
        self.logging.constructTestCase_Log(testMethod_OutputLog=method_Log)
        return isCritical

    def applicationDetails(self):
        pass

    def get_Test_Modules(self, test_ConditionsList):
        testModules = []
        for pom_Item in test_ConditionsList:
            for testName in pom_Item.keys():
                testModules.append(testName)

        return testModules

    def getVarDictionary(self, test_Conditions):
        tempDict = {}
        for key, val in test_Conditions.items():
            tempDict[key] = val
        return tempDict

    def setUpTestData(self):
        if self.isDataPrepRequired:
            print(f'{bcolors.BITALIC}{bcolors.ERR}{bcolors.BLUE}*Setting up test data...{bcolors.END}')

            try:
                if os.name == 'nt':
                    target_DataPrep_Module = importlib.import_module(
                        fr'{self.testDefinitionsDir}.{r"__dataPrep__"}'.replace("\\", "."))
                else:
                    target_DataPrep_Module = importlib.import_module(
                        fr'{self.testDefinitionsDir}.{r"__dataPrep__"}'.replace("/", "."))
                return self.testDataDictionary_Setter(target_DataPrep_Module, self.pom_ConditionsList)
            except ModuleNotFoundError:
                print(
                    f'{bcolors.ERRMSG}{bcolors.ERR}In target test directory -> "{self.testDefinitionsDir}\n\t{bcolors.ITALIC}*No module called __dataPrep__.py found!"{bcolors.ENDC}')
                if self.logTestExceptions:
                    self.format_stacktrace()
                return False

    def testDataDictionary_Setter(self, target_DataPrep_Module, pom_ConditionsList):
        try:
            target_DataPrep_Class = getattr(target_DataPrep_Module, 'DataPreparation')
            ##Creates an instance based on above class, and initializating it's contructor
            dataPrep_Instance = target_DataPrep_Class(pom_ConditionsList)
            newTestDataDictionary = {}
            import inspect, copy
            from builtins import filter
            attrs = (getattr(dataPrep_Instance, name) for name in dir(dataPrep_Instance))
            methods = filter(inspect.ismethod, attrs)
            for method in list(methods)[1:]:
                # specificTestData = getattr(dataPrep_Instance, )()
                specificTestData = method()
                try:

                    if type(specificTestData) == dict:
                        newTestDataDictionary = {}
                        for key, value in specificTestData.items():
                            # newTestDataDictionary = copy.deepcopy(self.testDataDictionary)
                            newTestDataDictionary[key] = value
                        if 'NA' in list(newTestDataDictionary.keys()):
                            return False
                        else:
                            self.testDataDictionarySetter(newTestDataDictionary)
                    else:
                        print(
                            f'{bcolors.WARN}{bcolors.ITALIC}No data retrived from {method.__name__}, method did not return a dictionary!{bcolors.ENDC}')
                except TypeError:
                    if self.logTestExceptions:
                        self.format_stacktrace()
                    # Can't handle methods with required arguments.

        except AttributeError:
            print(
                f'{bcolors.ERRMSG}{bcolors.ERR}{bcolors.ITALIC}*No CLASS called "DataPreparation" found in __dataPrep__.py\n\t*Target Dir -> "{self.testDefinitionsDir}{self.utils.format_stacktrace()}"{bcolors.ENDC}')
            if self.logTestExceptions:
                self.format_stacktrace()
            return False
        except TypeError as TypeErr:
            print(
                f'{bcolors.ERRMSG}{bcolors.ERR}{bcolors.ITALIC}Failed to get test data, stack trace below:\n\t*Hint: Constructor initialization mistake, check if any input param is instantiated/required, should have one in DataPreparation __init__\n{self.utils.format_stacktrace()}{bcolors.ENDC}')
            if self.logTestExceptions:
                self.format_stacktrace()
            return False

    def testDataDictionarySetter(self, testDataDictionary):
        if len(testDataDictionary) > 0:
            self.testDataDictionary = testDataDictionary

    def itsMGA(self):
        try:
            import bcolors
            import random
            if random.randint(1, 220) == random.randint(1, 202):
                print(f'{bcolors.OK}{bcolors.BOLD}{bcolors.ITALIC}Hey, its MGA 👋{bcolors.ENDC}')
        except Exception:
            pass

    def format_stacktrace(self):
        import traceback, sys, os
        parts = ["Traceback (most recent call last):\n"]
        parts.extend(traceback.format_stack(limit=25)[:-2])
        parts.extend(traceback.format_exception(*sys.exc_info())[1:])
        print(f'{bcolors.WARN}{bcolors.BOLD}{"".join(parts)}{bcolors.END}')
        return "".join(parts)

    def createOutputStructure(self):
        filesList = ["/Output/TraceFiles/.gitkeep", "/Output/ScreenShots/.gitkeep",
                     "/Output/ScreenShots/ScreenShots.txt"]
        sourceDirName = os.path.dirname(__main__.__file__)

        for file in filesList:
            os.makedirs(os.path.dirname(sourceDirName + file), exist_ok=True)
            with open(sourceDirName + file, "w") as f:
                f.write("")

    @staticmethod
    def get_project_root() -> Path:
        return Path(__file__).parent.parent

    def get_myTestSuit(self):
        if type(self.testSuit) == str:
            return self.utils.getTestSuit(testSuiteLocation=self.testSuit)
        elif type(self.testSuit) == list:
            return self.testSuit
        else:
            return "Invalid testSuit"







    def testName_setter(self, testName):
        self.test_Name = testName


if __name__ == "__main__":
    pass
