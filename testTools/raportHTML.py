from json2html import *
from pathlib import Path
from testTools.utilities import utilities as utilities
import os, __main__
import os

utils = utilities()

def generateRaport(resultD, testName, value, retryNo=1, trustedList = None):
    sourceDirName = os.path.dirname(__main__.__file__)

    flag, reason = raportValidator(resultD, testName, trustedList=trustedList)

    try:
        screenShotsPath = utils.to_posix_full_path('\\Output\\ScreenShots')

        raportPath = utils.to_posix_full_path('\\Output\\')

        dirPath = utils.to_posix_full_path('\\SRC\\raportHTML_pkg\\')
        with open(sourceDirName + utils.to_posix_path('\\Output\\ScreenShots\\ScreenShots.txt'), 'r') as opened_file:
            screenShotsList = opened_file.readlines()

        screenShotsList = list(map(lambda s:s.strip(),screenShotsList))

        if flag == False:
            status = resultD[testName]['Status']

        elif resultD[testName]['Status'] != 'EXECUTION_ABORTED':
            resultD[testName]['Status'] = 'Invalid_TestExecution'
            status = 'Invalid_TestExecution'
            resultD[testName].update({'FAULT':'Invalid_TestExecution_REASON --> [{}]'.format(reason)})
        else:
            status = resultD[testName]['Status']
        b = json2html.convert(json=resultD)
        # open, write and close html file for each test
        raportName = str(Path(sourceDirName + '/Output')) + '/' + status + '-' + testName + ".html"
        open_html = open(raportName, "w", encoding="utf-8")
        open_html.write(b)
        open_html.close()
        # open, read and close html file for each test and replace html file

        read_html = open(raportName, "r")

        contain = read_html.read()
        contain = contain.replace("<th>Status</th><td>FAIL</td>",
                                  "<th>Status</th><td style='background-color:Orange;'>FAILED</td>") \
            .replace("<th>Status</th><td>PASSED</td>",
                     "<th>Status</th><td style='background-color:MediumSeaGreen;'>PASSED</td>") \
            .replace("<th>Status</th><td>CRITICAL_FAIL</td>",
                     "<th>Status</th><td style='background-color:Tomato;'>CRITICAL_FAIL</td>") \
            .replace("<th>Status</th><td>Invalid_TestExecution</td>",
                     "<th>Status</th><td style='background-color:Tomato;'>Invalid_TestExecution</td>") \
            .replace("<th>Status</th><td>INTERNAL_CODE_ERROR</td>",
                     "<th>Status</th><td style='background-color:Tomato;'>INTERNAL_CODE_ERROR</td>") \
            .replace("test_page_Login", "Login") \
            .replace("test_page_Search", "Search") \
            .replace("test_page_Dashboard", "Dashboard") \
            .replace("test_page_configureaza", "Configureaza Oferte") \
            .replace("test_page_detalii_Utilizator_Si_Numar", "Utilizator si Numar") \
            .replace("test_page_detalii_UtilizatorTitular", "Titual Cont") \
            .replace("test_page_Detail_GDPR", "G.D.P.R.") \
            .replace("test_page_Cont_Nou", "Cont Nou") \
            .replace("test_page_SendToVOS", "Trimite in VOS") \
            .replace(testName.replace('&', '&amp;'),
                     testName + "<br><pre style='text-align:left;'>" + ' ' + str(value).replace("{",
                                                                                           "").replace(
                         "}", "").replace(",", ",\n") + "</pre>")

        with open(str(Path(sourceDirName + '/testTools')) + utils.to_posix_path('\\enrichHTML.txt'), 'r') as opened_file:
            contain = opened_file.read().replace('ToBeReplaced', contain).replace('&amp;', '&')
        for i in screenShotsList:
            i = i.replace('\n', '')
            # print("replace string",  utils.to_posix_path(sourceDirName + "\\Output\\").replace('/', '\\')))
            contain = contain.replace(i, '<br><img src="{}" alt="Snow" style="width:20%" onclick="myFunction(this);">'.format(i.replace((sourceDirName + utils.to_posix_path("\\Output\\")).replace('/', '\\'),'')))
        read_html.close()
        expectedList = []

        # open, write and close html file for each test
        with open(raportName, 'w') as opened_file:            opened_file.write(str(contain))

        # if 'critical' in raportName.lower():
        #     utils.setTempValue('RaportName', value=raportName, updateLst=True)

        return {testName: status}


    except FileNotFoundError as e:
        if retryNo == 1:
            with open(Path(sourceDirName + '/Output/ScreenShots/ScreenShots.txt'), 'w') as opened_file:
                opened_file.write('')
        else:
            print(e)
            generateRaport(resultD=resultD, testName=testName, value=value, retryNo=retryNo-1)


def raportValidator(result, testName, trustedList):

    flag = False
    reason = ''

    keysFromRaport = []

    for key, value in result[testName]['Tests Execution Log'].items():
        if key in trustedList:
            keysFromRaport.append(key)

        if len(value['ExecutionLog']) == 0:
            flag = True
            reason = 'For {} step, no execution found'.format(key)

    if len(trustedList) != len(keysFromRaport):
        flag = True
        reason = 'Execution steps not fulfilled => [{}]'.format(list(set(trustedList) - set(keysFromRaport)))

    if result[testName]['Status'] == 'CRITICAL_FAIL':

        for i in range(len(keysFromRaport)):
            if trustedList[i] == keysFromRaport[i]:
                flag = False
            else:
                flag = True

    return False, reason



if __name__ == '__main__':
    resultD = {'Sales channels=Retail (Franciza & Store) Login=END Search by=MSISDN Customer found=True Flow type=Click on Numar Nou(38)_DEXAutomation': {'Status': 'CRITICAL_FAIL', 'HOST Name': 'Windows OS', 'Release': 'None Given, Hardcoded', 'EXECUTION_TS': '20200917131213', 'Tests Execution Log': {'test_page_Login': {'Status': 'CRITICAL_FAIL', 'ExecutionLog': {'Click_OK': 'PASSED', 'Type_UserName': 'FAIL --> [exceptie Message: \n pentru xPath : //input[@name="username1"]\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)] ScreenShots/Sales channels=Retail (Franciza & Store) Login=END Search by=MSISDN Customer found=True Flow type=Click on Numar Nou(38)_UserName - test_page_Login.png', 'Get Attribute Value_Password': 'PASSED', 'Type_Password': 'PASSED', 'Click_Login': 'PASSED', 'Select by index_DealerSelect': "FAIL --> [exceptie Message: \n pentru xPath : //select[@data-automation-id='select-dealer']\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)] ScreenShots/Sales channels=Retail (Franciza & Store) Login=END Search by=MSISDN Customer found=True Flow type=Click on Numar Nou(38)_DealerSelect - test_page_Login.png", 'Click_Continua': "CRITICAL_FAIL --> [exceptie Message: \n pentru xPath : //button[@data-automation-id='dealer-login-button']\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 10 sec de wait (TimeoutException)] ScreenShots/Sales channels=Retail (Franciza & Store) Login=END Search by=MSISDN Customer found=True Flow type=Click on Numar Nou(38)_Continua - test_page_Login.png"}}}}}
    testName = 'Sales channels=Retail (Franciza & Store) Login=END Search by=MSISDN Customer found=True Flow type=Click on Numar Nou(38)_DEXAutomation'
    value = {'channel': 'RETAIL', 'searchType': 'MSISDN', 'searchValue': 733044811, 'configType': 'ACHIZITIE', 'abNouType': 'GA', 'scenarioNameDC': 'CR_True_Client_Matur', 'offerChoice': 'ConfigOferte', 'scenarioName_PC': 'OferteConfigurabile_S2D_Rate', 'scenarioName_Utl_Nr': 'Scenariu_Numar_Random', 'scenarioName_isTitular': 'Utilizator_titular_Yes', 'scenarioName_GDPR': 'Scenariu_GDPR_allYES', 'scenarioContNou': 'Is_Cont_Nou_Required_Yes_FacturaElectr'}

    # generateRaport(resultD, testName, value)