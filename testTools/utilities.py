import bcolors, sys
import smtplib
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import NoSuchElementException, JavascriptException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import StaleElementReferenceException
import time
import requests
import json
from workalendar.europe import Romania
import datetime
from selenium.webdriver.common.keys import Keys
from testTools.loggingMechanism import VDFLogging
import xlsxwriter
import os, ast
import pathlib
import pymsteams
import posixpath
import re
import traceback
import pandas as pd
from pathlib import Path
from selenium.webdriver.common.action_chains import ActionChains

class tempValue:

    tempDict = {'Tempdict': {}}

    @classmethod
    def setTempValue(cls, key, value, updateLst=False):
        if updateLst == False:
            cls.tempDict['Tempdict'].update({key: value})
        else:
            cls.tempDict['Tempdict'][key].append(value)

    @classmethod
    def getTempValue(cls, key):
        try:
            return cls.tempDict['Tempdict'][key]
        # except KeyboardInterrupt:
        #     pass
        except (ValueError, KeyError) as e:
            print("getTempValue : Value not found for given key = {}".format(key))


class utilities(tempValue):

    VDFloggingObj = VDFLogging()
    stepNumber = 0
    terminalLog = False
    globalWait = 0.2

    def __init__(self, callingObj = object, browser=None, testCaseName=None, xPaths_Dictionary=None, inputDictionary=None):
        self.inputDictionary = inputDictionary
        self.browser = browser
        if callingObj.__class__.__name__ != 'type':
            self.VDFloggingObj.__init__(testMethod_Name=callingObj.__class__.__name__, xPaths_Dictionary=xPaths_Dictionary, browser=browser, testCaseName=testCaseName, inputDictionary=inputDictionary)
        ## Alternatively instead of callingOBj => self.format_stacktrace().split('File')[-1].split(".py")[0].split('\\')[-1]


    def format_stacktrace(self):
        parts = ["Traceback (most recent call last):\n"]
        parts.extend(traceback.format_stack(limit=25)[:-2])
        parts.extend(traceback.format_exception(*sys.exc_info())[1:])
        return "".join(parts)

    def writeToCSV(self, statusList, text2Teams):
        try:
            with xlsxwriter.Workbook('TestCaseOp/raport.xlsx') as workbook:
                worksheet = workbook.add_worksheet()
                worksheet.write_row(0, 0, ['TestName'])
                worksheet.write_row(0, 1, ['Status'])
                count = 0
                for row_num, data in enumerate(statusList):
                    count +=1
                    row_num += 1
                    lst = data.split(" - ")
                    worksheet.write_row(row_num, 0, lst)
            read_file = pd.read_excel('TestCaseOp/raport.xlsx',engine='openpyxl')
            read_file.to_csv('TestCaseOp/raport.csv', index=None, header=True)

            self.outlookConnectorMessanger(text2Teams + '\nRaport CSV generat la data de {} continand {} - teste</pre>'.format(datetime.datetime.now(), count))
        except Exception as e:
            self.outlookConnectorMessanger(text2Teams + 'Failed to writeToCSV because {}'.format(e))
            print('Failed to writeToCSV because {}'.format(e))
            
    def outlookConnectorMessanger(self, messageToSend, retry=3):
        try:
            myTeamsMessage = pymsteams.connectorcard(
                "https://outlook.office.com/webhook/53d8ed16-ee34-4b0c-b097-da528400454d@68283f3b-8487-4c86-adb3-a5228f18b893/IncomingWebhook/f67c060e7aac4bfa8d8003e043f13390/cec843cf-af1e-434f-9287-1cc57c7e29f0",
                http_proxy="http://pac.internal.vodafone.com/fcproxy.pac",
                https_proxy="http://10.140.206.118:3128")

            myTeamsMessage.text(messageToSend)
            myTeamsMessage.send()
        except Exception as e:
            print('Connection error to Teams Connector\n', e)
            if retry <= 3 and retry > 0:
                self.outlookConnectorMessanger(messageToSend, retry=retry-1)

    def is_business_day(self, date, plus=None):

        calendar = Romania()
        calendar.holidays(2020)
        if plus == None:
            plus = 0
        date = (datetime.datetime.today() + datetime.timedelta(days=plus))

        return calendar.is_working_day(date), date

    def wait_loadingCircle(self, enable=False):
        # time.sleep(20)
        if enable == True:
            try:
                while True:
                    self.browser.find_element_by_xpath('//div[@class="sk-circle-container"]')
            except:
                if(self.terminalLog == True):print('circle byebye')

    def upload_document(self, xpath, filePath, priority=None, externalMethod=None, enable_Logging=True,
                        customTimeout=None, timeout=True, retryNo=1, element_name=None):

        customTimeout = self.isGlobalWait(customTimeout)

        actionType = None
        if externalMethod != None:
            actionType = externalMethod
        else:
            actionType = "Upload a document"

        if enable_Logging == False:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.presence_of_element_located((By.XPATH, xpath)))

                # upload the file
                upload = self.browser.find_element_by_xpath(xpath)
                upload.send_keys(filePath)

                return True

            except(NoSuchElementException, StaleElementReferenceException) as e:
                if (self.terminalLog == True): print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath, e))
                return False
            except(TimeoutException) as e_timeout:
                if (self.terminalLog == True): print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {} sec de wait (TimeoutException)'.format(
                        str(e_timeout), xpath, customTimeout))
                return False

        elif enable_Logging:
            try:

                WebDriverWait(self.browser, customTimeout).until(ec.presence_of_element_located((By.XPATH, xpath)))

                # upload the file
                upload = self.browser.find_element_by_xpath(xpath)
                upload.send_keys(filePath)

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority,
                                                         action_type=actionType,
                                                         isException=False, browser=self.browser)

                return True
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if (self.terminalLog == True): print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath,
                                                                                       e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority,
                                                         action_type=actionType,
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xpath,
                                                             e), browser=self.browser)
                return False
            except(Exception) as e_timeout:
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority,
                                                         action_type=actionType,
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {} sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xpath, customTimeout),
                                                         browser=self.browser)
                if (self.terminalLog == True): print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {} sec de wait (TimeoutException)'.format(
                        str(e_timeout), xpath, customTimeout))
                return False

    def get_text(self, xPath, priority=None, enable_Logging = True, customTimeout = None, timeout=True, retryNo=1, element_name=None):

        customTimeout = self.isGlobalWait(customTimeout)


        time.sleep(0.5)
        if enable_Logging == True:

            try:
                if timeout == True:
                    WebDriverWait(self.browser, customTimeout).until(ec.presence_of_element_located((By.XPATH, xPath)))

                result = self.browser.find_element_by_xpath(xPath).text
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='Get element Text',
                                                         isException=False, browser=self.browser)
                return result

            except(NoSuchElementException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='Get element Text',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xPath, e), browser=self.browser)

                return False

            except(StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='Get element Text',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xPath, e), browser=self.browser)

                if retryNo == 1:
                    self.get_text(xPath, priority=priority, enable_Logging=enable_Logging, customTimeout=customTimeout, timeout=timeout,
                                 retryNo=retryNo-1)
                else:
                    return False

            except(Exception) as e_timeout:
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='Get element Text',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {} sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath, customTimeout), browser=self.browser)
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {} sec de wait (TimeoutException)'.format(
                        str(e_timeout), xPath, customTimeout))
                return False

        elif enable_Logging == False:
            try:
                WebDriverWait(self.browser, 10).until(ec.visibility_of_element_located((By.XPATH, xPath)))

                return self.browser.find_element_by_xpath(xPath).text
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e))
                return False
            except(Exception) as e_timeout:

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)

    # returns the number of elements from a list of elements found by XPATH
    def get_numberOfElements(self, xPath, priority=None, enable_Logging = True, customTimeout = None, timeout=True, retryNo=1, element_name=None):
            
        customTimeout = self.isGlobalWait(customTimeout)


        time.sleep(0.5)
        if enable_Logging == True:

            try:
                if timeout == True:
                    WebDriverWait(self.browser, customTimeout).until(ec.presence_of_element_located((By.XPATH, xPath)))

                result = self.browser.find_elements_by_xpath(xPath)
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='Get number of elements for',
                                                         isException=False, browser=self.browser)
                return len(result)

            except(NoSuchElementException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='Get number of elements for',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xPath, e), browser=self.browser)

                return False

            except(StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='Get number of elements for',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xPath, e), browser=self.browser)

                if retryNo == 1:
                    self.get_text(xPath, priority=priority, enable_Logging=enable_Logging, customTimeout=customTimeout, timeout=timeout,
                                 retryNo=retryNo-1)
                else:
                    return False

            except(Exception) as e_timeout:
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='Get number of elements',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {} sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath, customTimeout), browser=self.browser)
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {} sec de wait (TimeoutException)'.format(
                        str(e_timeout), xPath, customTimeout))
                return False

        elif enable_Logging == False:
            try:
                WebDriverWait(self.browser, 10).until(ec.visibility_of_element_located((By.XPATH, xPath)))

                return self.browser.find_element_by_xpath(xPath).text
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e))
                return False
            except(Exception) as e_timeout:

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)


    def get_attrValue(self, xPath, attrName, priority=None, enable_Logging=True, retryNo=1, element_name=None, customTimeout = None):
        customTimeout = self.isGlobalWait(customTimeout)


        if enable_Logging:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.visibility_of_element_located((By.XPATH, xPath)))
                elementByXPATH = self.browser.find_element_by_xpath(xPath)

                result =  elementByXPATH.get_attribute(attrName)

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='Get Attribute Value',
                                                    isException=False, browser=self.browser)
                return result
            except(NoSuchElementException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Get Attribute Value',
                                                         isException=True, exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e), browser=self.browser)
                return False
            except(StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Get Attribute Value',
                                                         isException=True, exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e), browser=self.browser)

                if retryNo == 1:
                    self.get_attrValue(xPath=xPath, attrName=attrName, priority=priority, enable_Logging=enable_Logging, retryNo=retryNo-1)
                else:
                    return False

            except(Exception) as e_timeout:
                
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Get Attribute Value',
                                                         isException=True, exeption=f'exceptie {str(e_timeout)} pentru xPath : {xPath}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {customTimeout} sec de wait (TimeoutException)', browser=self.browser)
                return False


        elif enable_Logging == False:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.visibility_of_element_located((By.XPATH, xPath)))
                elementByXPATH = self.browser.find_element_by_xpath(xPath)

                result = elementByXPATH.get_attribute(attrName)

                return result
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath,
                                                                                       e))
                return False
            except(Exception) as e_timeout:

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)
        # if elementByXPATH != None:
        #     return elementByXPATH.get_attribute(attrName)

    def is_element_Displayed(self, xPath, priority=None, enable_Logging=True, element_name=None, customTimeout = None):
        customTimeout = self.isGlobalWait(customTimeout)


        if enable_Logging:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.visibility_of_element_located((By.XPATH, xPath)))
                result = self.browser.find_element_by_xpath(xPath).is_displayed()
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=False, browser=self.browser)
                return result
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xPath, e), browser=self.browser)
                return False
            except(Exception) as e_timeout:
                
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)
                return False
        elif enable_Logging == False:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.visibility_of_element_located((By.XPATH, xPath)))
                return self.browser.find_element_by_xpath(xPath).is_displayed()
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath,
                                                                                       e))
                return False
            except(Exception) as e_timeout:

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)
                return False

    def is_element_Enabled(self, xPath, priority=None, enable_Logging=True, element_name=None, customTimeout = None):
        customTimeout = self.isGlobalWait(customTimeout)


        if enable_Logging:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.visibility_of_element_located((By.XPATH, xPath)))
                result = self.browser.find_element_by_xpath(xPath).is_enabled()
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='is Enabled',
                                                         isException=False, browser=self.browser)
                return result
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath,
                                                                                       e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='is Enabled',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xPath,
                                                             e), browser=self.browser)
                return False
            except(Exception) as e_timeout:
                
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='is Enabled',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)
                return False
        elif enable_Logging == False:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.visibility_of_element_located((By.XPATH, xPath)))
                return self.browser.find_element_by_xpath(xPath).is_enabled()
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath,
                                                                                       e))
                return False
            except(Exception) as e_timeout:

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)
                return False

    def element_exists(self, xPath, enable_Logging=True, priority=None, silent=None, element_name=None, customTimeout = None):
        customTimeout = self.isGlobalWait(customTimeout)


        if enable_Logging:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.presence_of_element_located((By.XPATH, xPath)))
                self.browser.find_element_by_xpath(xPath)
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='Exista elementul',
                                                         isException=False, browser=self.browser)
                return True
            except(NoSuchElementException, StaleElementReferenceException) as e:
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='Exista elementul',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xPath, e), browser=self.browser)
                if silent is None:
                    if(self.terminalLog == True):print(
                        'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e))
                else:
                    return False
            except(Exception) as e_timeout:
                if xPath != '//button[@data-automation-id="refresh-error-page"]':

                    utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='Exista elementul',
                                                             isException=True,
                                                             exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                                 str(e_timeout), xPath), browser=self.browser)
                    if(self.terminalLog == True):print(
                        'exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                            str(e_timeout), xPath))
                    return False
                else:
                    if(self.terminalLog == True):print('Loading animation validation passed')

        elif enable_Logging == False:
            try:
                # WebDriverWait(self.browser, 10).until(ec.visibility_of_element_located((By.XPATH, xPath)))
                self.browser.find_element_by_xpath(xPath)
                return True
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if xPath != '//div[@class="sk-circle-container"]':
                    if(self.terminalLog == True):print(
                        'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath,
                                                                                           e))
                return False
            except(Exception) as e_timeout:
                if xPath != '//div[@class="sk-circle-container"]':
                    if(self.terminalLog == True):print(
                        'exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                            str(e_timeout), xPath))
                return False

    def is_element_Selected(self, xPath, priority=None, enable_Logging=True, element_name=None, customTimeout = None):
        customTimeout = self.isGlobalWait(customTimeout)


        if enable_Logging:
            try:

                WebDriverWait(self.browser, customTimeout).until(ec.visibility_of_element_located((By.XPATH, xPath)))
                result = self.browser.find_element_by_xpath(xPath).is_selected()
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Get Attribute Value',
                                                         isException=False, browser=self.browser)
                return result
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath,
                                                                                       e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Get Attribute Value',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xPath,
                                                             e), browser=self.browser)
                return False
            except(Exception) as e_timeout:
                
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Get Attribute Value',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)
                return False
        elif enable_Logging == False:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.visibility_of_element_located((By.XPATH, xPath)))
                return self.browser.find_element_by_xpath(xPath).is_selected()
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath,
                                                                                       e))
                return False
            except(Exception) as e_timeout:

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)
                return False

    def type_js(self, xpath, value, enter=None, priority=None, enable_Logging=True, js=None, classValue=None, forLoopPause=0.1, type_Token=False, recursionLimit = 0, retryNo = 1, element_name=None, enterWait=0.01, tab = False, customTimeout = None):
        customTimeout = self.isGlobalWait(customTimeout)

        if enterWait > 0.01 and enter == None:
            enter = True

        if isinstance(value, int) == True:
            value = str(value)

        if enable_Logging == True:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.element_to_be_clickable((By.XPATH, xpath)))
                elemnt = self.browser.find_element_by_xpath(xpath)
                elemnt.clear()

                if type_Token == True:
                    elemnt.click()
                    for i in list(value):
                        # elemnt.send_keys(i)
                        keyboard = Controller()
                        keyboard.press(str(i))
                        keyboard.release(str(i))
                        time.sleep(1)
                else:
                    if js == True:
                        for i in list('{}'.format(value)):
                            self.browser.execute_script("document.evaluate('" + xpath + "', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value = '" + value + "'")
                            # self.browser.find_element_by_xpath("{}".format(xpath)))
                            if enter != None:
                                elemnt.send_keys(Keys.ENTER)
                    else:
                        for i in list('{}'.format(value)):
                            elemnt.send_keys(i)
                            time.sleep(forLoopPause)

                        if enter != None:
                            time.sleep(enterWait)
                            elemnt.send_keys(Keys.ENTER)
                        if tab != None:
                            time.sleep(enterWait)
                            elemnt.send_keys(Keys.TAB)                            

                    attrValue = self.get_attrValue(xPath=xpath, attrName='value', enable_Logging=False)
                    if recursionLimit <= 2 and recursionLimit > 0 and  attrValue != value:
                        print('Recursively', recursionLimit)

                        self.type_js(xpath=xpath, value=value, enter=enter, priority=priority, enable_Logging=enable_Logging, js=js,
                                classValue=classValue, forLoopPause=forLoopPause, type_Token=type_Token, recursionLimit=recursionLimit-1)

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Type',
                                                         isException=False, browser=self.browser)

            except(NoSuchElementException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath,
                                                                                       e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Type',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xpath,
                                                             e), browser=self.browser)
                return False


            except(StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath,
                                                                                       e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Type',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xpath,
                                                             e), browser=self.browser)
                if retryNo == 1:
                    self.type_js(xpath=xpath, value=value, enter=enter, priority=priority, enable_Logging=enable_Logging, js=js,classValue=classValue, forLoopPause=forLoopPause, type_Token=type_Token, recursionLimit=recursionLimit, retryNo=retryNo-1)
                else:
                    return False


            except Exception as e_timeout:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 20 sec de wait (TimeoutException)'.format(
                        str(e_timeout), xpath))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Type',
                                                         isException=True, exeption=
                                                         'exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xpath), browser=self.browser)

        elif enable_Logging == False:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.visibility_of_element_located((By.XPATH, xpath)))
                elemnt = self.browser.find_element_by_xpath(xpath)
                elemnt.clear()
                for i in list('{}'.format(value)):
                    elemnt.send_keys(i)
                    time.sleep(0.1)
                if enter != None:
                    elemnt.send_keys(Keys.ENTER)

            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath,
                                                                                       e))
                return False
            except(Exception) as e_timeout:

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)

    def click_js(self, xpath, priority=None, enable_Logging=True, externalMethod=None, customTimeout = None, js=True, element_name=None):

        customTimeout = self.isGlobalWait(customTimeout)

        actionType = None
        if externalMethod != None:
            actionType = externalMethod
        else:
            actionType = "Click"


        if enable_Logging == False:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.element_to_be_clickable((By.XPATH, xpath)))
                if js == True:
                    self.browser.execute_script("arguments[0].click();", self.browser.find_element_by_xpath(xpath))
                else:
                    self.browser.find_element_by_xpath(xpath).click()
                return True

            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath, e))
                return False
            except(Exception) as e_timeout:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {} sec de wait (TimeoutException)'.format(
                        str(e_timeout), xpath, customTimeout))
                return False

        elif enable_Logging:
            try:

                WebDriverWait(self.browser, customTimeout).until(ec.element_to_be_clickable((By.XPATH, xpath)))

                if js:
                    self.browser.execute_script("arguments[0].click();", self.browser.find_element_by_xpath(xpath))
                else:
                    self.browser.find_element_by_xpath(xpath).click()

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type=actionType,
                                                         isException=False, browser=self.browser)

                # utilities.VDFloggingObj.terminalLog(testMethod_Name=inspect.currentframe().f_code.co_name, elementName=element_name, priority=priority, exceptionMessage="", actionStatus="PASSED", currentExecLog=self.inputDictionary)
                return True
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath,
                                                                                       e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type=actionType,
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xpath,
                                                             e), browser=self.browser)
                return False
            except(Exception) as e_timeout:
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type=actionType,
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {} sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xpath, customTimeout), browser=self.browser)
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {} sec de wait (TimeoutException)'.format(
                        str(e_timeout), xpath, customTimeout))
                return False

    def moveToNewTab(self, index, tabName):
        try:
            self.browser.switch_to.window(self.browser.window_handles[index])
            utilities.VDFloggingObj.priority_handler(element_name=tabName, xpath=index, priority=None,
                                                     action_type='switch_Tab',
                                                     isException=False, browser=self.browser)
            return True
        except Exception as e:
            utilities.VDFloggingObj.priority_handler(element_name=tabName, xpath=index, priority=None,
                                                     action_type='switch_Tab',
                                                     isException=True,
                                                     exeption='exceptie {} la schimbarea tabului : {}\nException Stack Trace : {}'.format(
                                                         str(e), tabName, e), browser=self.browser)
            return False

    def dropDownSelector_Index(self, xpath, index, priority=None, enable_Logging=True, element_name=None, customTimeout = None):
        customTimeout = self.isGlobalWait(customTimeout)

        if enable_Logging:
            try:
                element = WebDriverWait(self.browser, customTimeout).until(ec.element_to_be_clickable((By.XPATH, xpath)))
                Select(element).select_by_index(index)
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Select by index',
                                                         isException=False, browser=self.browser)
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath, e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Select by index',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xpath, e), browser=self.browser)
                return False
            except(Exception) as e_timeout:
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Select by index',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xpath), browser=self.browser)
                
        elif enable_Logging == False:
            try:
                element = WebDriverWait(self.browser, customTimeout).until(ec.element_to_be_clickable((By.XPATH, xpath)))
                Select(element).select_by_index(index)
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath, e))
                return False
            except(Exception) as e_timeout:

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)

    def check_button(self, xpath, xpathCLick=None, value=None, priority=None, enable_Logging=True, element_name=None, customTimeout = None):
        customTimeout = self.isGlobalWait(customTimeout)


        if enable_Logging:
            result = None
            try:
                if xpathCLick == None:
                    xpathCLick = xpath
                WebDriverWait(self.browser, customTimeout).until(ec.presence_of_element_located((By.XPATH, xpath)))
                result = self.browser.find_element_by_xpath(xpath).get_attribute(value)
                if result == 'false':
                    utilities.click_js(self, xpathCLick, enable_Logging=True, externalMethod='Check')
                else:
                    utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpathCLick, priority=priority, action_type='Check',
                                                             isException=False, browser=self.browser)

            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath,
                                                                                       e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Check',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xpath,
                                                             e), browser=self.browser)
                return False
            except(Exception) as e_timeout:
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Check',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xpath), browser=self.browser)
                
                return False
        elif enable_Logging == False:
            result = None
            try:
                if xpathCLick == None:
                    xpathCLick = xpath
                WebDriverWait(self.browser, customTimeout).until(ec.presence_of_element_located((By.XPATH, xpath)))
                result = self.browser.find_element_by_xpath(xpath).get_attribute(value)

                if result == 'false':
                    utilities.click_js(self, xpathCLick, enable_Logging=False, externalMethod='Check')

            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath, e))
                return False
            except(Exception) as e_timeout:

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)

        # if readio button is checked then uncheck

    def uncheck_button(self, xpath, xpathCLick=None, value=None, priority=None, enable_Logging=True, element_name=None, customTimeout = None):
        customTimeout = self.isGlobalWait(customTimeout)


        if enable_Logging:
            result = None
            try:
                if xpathCLick == None:
                    xpathCLick = xpath

                WebDriverWait(self.browser, customTimeout).until(ec.presence_of_element_located((By.XPATH, xpath)))
                result = self.browser.find_element_by_xpath(xpath).get_attribute(value)
                print(result)

                if result == 'true':
                    utilities.click_js(self, xpathCLick, enable_Logging=True)
                else:
                    utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Uncheck',
                                                             isException=False, browser=self.browser)


            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath,
                                                                                       e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Uncheck',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(e), xpath,
                                                             e), browser=self.browser)
                return False

            except(Exception) as e_timeout:
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Uncheck',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xpath), browser=self.browser)
                
                return False

            except(JavascriptException) as f:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(f), xpath,
                                                                                       f))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Uncheck',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                             str(f), xpath,
                                                             f), browser=self.browser)
                return False

        elif enable_Logging == False:
            result = None
            try:
                if xpathCLick == None:
                    xpathCLick = xpath

                WebDriverWait(self.browser, customTimeout).until(ec.presence_of_element_located((By.XPATH, xpath)))
                result = self.browser.find_element_by_xpath(xpath).get_attribute(value)

                if result == 'true':
                    utilities.click_js(self, xpathCLick, enable_Logging=False)

            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath,
                                                                                       e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Uncheck',
                                                         isException=True, browser=self.browser)
                return False

            except(Exception) as e_timeout:
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Uncheck',
                                                         isException=True, browser=self.browser)
                
                return False

    def callRESTApi(self, url, key):
        try:
            req = requests.get(url)
            req.raise_for_status()
            iccd = json.loads(req.text)
            sim = iccd[key][0]
            return sim
        except requests.exceptions.ConnectionError as e:
            print('Connection Error' + str(e))
            return False
        except requests.exceptions.HTTPError as code:
            print('HTTPError ' + str(code))
            return False

    def get_with_URL_Params(self, url, key, env):
        try:
            req = requests.get(url+env)
            req.raise_for_status()
            iccd = json.loads(req.text)
            sim = iccd[0][key]
            return sim
        except requests.exceptions.ConnectionError as e:
            print('Connection Error' + str(e))
            return False
        except requests.exceptions.HTTPError as code:
            print('HTTPError ' + str(code))
            return False

    def dropDownSelector_By_visible_text(self, xpath, value, enable_Logging=True, priority=None, element_name=None, customTimeout = None):
        customTimeout = self.isGlobalWait(customTimeout)


        if enable_Logging:
            try:
                element = WebDriverWait(self.browser, customTimeout).until(ec.element_to_be_clickable((By.XPATH, xpath)))
                Select(element).select_by_visible_text(value)
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Select by index',
                                                         isException=False, browser=self.browser)
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath, e))
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Select by index',
                                                         isException=True, browser=self.browser)
                return False
            except(Exception) as e_timeout:
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority, action_type='Select by index',
                                                         isException=True, browser=self.browser)
                
        elif enable_Logging == False:
            try:
                element = WebDriverWait(self.browser, customTimeout).until(ec.element_to_be_clickable((By.XPATH, xpath)))
                Select(element).select_by_visible_text(value)
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if(self.terminalLog == True):print(
                    'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath, e))
                return False
            except(Exception) as e_timeout:

                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority,
                                                         action_type='Element Vizibil',
                                                         isException=True,
                                                         exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                                                             str(e_timeout), xPath), browser=self.browser)
                return False

    def scroll(self, nr_scrolls=1, element_name=None):
        a = 0
        while nr_scrolls != 0:
            # Get scroll height
            ### This is the difference. Moving this *inside* the loop
            ### means that it checks if scrollTo is still scrolling
            last_height = self.browser.execute_script("return document.body.scrollHeight")
            # Scroll down to bottom
            self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            # Wait to load page
            #t.sleep(2)
            utilities.wait_loadingCircle(self)
            # Calculate new scroll height and compare with last scroll height
            new_height = self.browser.execute_script("return document.body.scrollHeight")
            if new_height == last_height:
                # try again (can be removed)
                self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                # Wait to load page
                #t.sleep(2)
                utilities.wait_loadingCircle(self)
                # Calculate new scroll height and compare with last scroll height
                new_height = self.browser.execute_script("return document.body.scrollHeight")
                # check if the page height has remained the same
                if new_height == last_height:
                    # if so, you are done
                    break
                # if not, move on to the next loop
                else:
                    last_height = new_height
                    continue
            nr_scrolls -= 1
            a += 1
            print('Scroll count => ["{}"]'.format(a))

    def log_customStep(self, key, status):
        self.VDFloggingObj.log_customStep(key=key, status=status)
        self.VDFloggingObj.terminalLog(externalElementName=key, externalStatus=status, isCustom=True)

    def getToken(self, url, key):
        url = url + str(key)
        try:
            req = requests.get(url)
            req.raise_for_status()
            token = req.text.replace('"', '')
            return token
        except requests.exceptions.ConnectionError as e:
            print('Connection Error' + str(e))
            return False
        except requests.exceptions.HTTPError as code:
            print('HTTPError ' + str(code))
            return False

        # def getMethodLog(self):
        #
        #     if(self.terminalLog == True):print(self.testMethod_OutputLog)
        #     return self.testMethod_OutputLog

    def getChild(self, inputDictionary=None, key=None):

        if inputDictionary is None:
            inputDictionary = {'Not Instantiated': 'TBD'}

        try:
            return inputDictionary[key]
        except (KeyError, ValueError) as e:
            return None

    def retryAPI(self, API, URL, KEY=None):
        try:
            responseAPI = None
            if API == 'Token':

                tokenId = utilities.getToken(self, URL,KEY)

                count = 3
                while tokenId == False and count >= 0:
                    count -= 1
                    tokenId = utilities.getToken(self, URL,KEY)

                responseAPI = tokenId

                if responseAPI == False:
                    self.log_customStep('getToken - 3 retry', 'Fail')
                    if(self.terminalLog == True):print("'Custom Log --->> getToken - 3 retry', 'Fail'")


            elif API == 'SIM':

                tokenId = utilities.callRESTApi(self, url=URL, key='ICCID')

                count = 3
                while tokenId == False and count >= 0:
                    count -= 1
                    tokenId = utilities.callRESTApi(self, url=URL, key='ICCID')

                responseAPI = tokenId

                if responseAPI == False:
                    self.log_customStep('getSIM - 3 retry', 'Fail')
                    if(self.terminalLog == True):print("'getSIM - 3 retry', 'Fail'")

            elif API == 'Prepaid':

                tokenId = utilities.callRESTApi(self, url=URL, key='PreMSISDN')

                count = 3
                while tokenId == False and count >= 0:
                    count -= 1
                    tokenId = utilities.callRESTApi(self, url=URL, key='PreMSISDN')

                responseAPI = tokenId

                if responseAPI == False:
                    self.log_customStep('getPreapid - 3 retry', 'Fail')
                    if(self.terminalLog == True):print("'getPreapid - 3 retry', 'Fail'")

            elif API == 'simpleAPICall':
                try:
                    req = requests.get(URL)
                    req.raise_for_status()
                    responseAPI = req.text
                    # if(self.terminalLog == True):print(responseAPI)
                    return responseAPI

                except requests.exceptions.ConnectionError as e:
                    print('Connection Error' + str(e))
                    return False
                except requests.exceptions.HTTPError as code:
                    print('HTTPError ' + str(code))
                    return False

            responseAPI = responseAPI.strip("\n")
            return responseAPI

        except (AttributeError, ValueError) as e:
            print('Attribute error exception retryAPI,\n "EXCEPTION --> {}"'.format(e))

    def resultValidation(self, resultDict, mandatoryKeys):
        if(self.terminalLog == True):print('rslt dict ', resultDict, mandatoryKeys)
        faultKeys = []
        for key in resultDict.keys():
            # if(self.terminalLog == True):print(key)
            if key in mandatoryKeys:
                # print(key)
                faultKeys.append(key)
            else:
                for childKey in key:
                    if len(childKey) == 0:
                        if childKey not in faultKeys:
                            faultKeys.append(key)

        return list(set(mandatoryKeys) - set(faultKeys))

    def scroll_to_element(self, xpathCLick, initialScroll=4, maxScroll=10, defaultClick=True, element_name=None):
        xpathCLick = self.retentionCustomPath(xpathCLick)
        if defaultClick == True:
            self.scroll(nr_scrolls=initialScroll)
            if self.element_exists(xPath=xpathCLick, silent=True) is True:
                self.click_js(xpathCLick)
            else:
                no_scrols = initialScroll
                while self.element_exists(xPath=xpathCLick, silent=True) is False:
                    self.scroll(nr_scrolls=1)
                    no_scrols += 1
                    if self.element_exists(xPath=xpathCLick, silent=True) is True:
                        break
                    if(self.terminalLog == True):print(no_scrols)
                    if no_scrols > maxScroll:
                        self.log_customStep('Nu se poate selecta o resursa in {}'.format('10'), 'CRITICAL_FAIL')
                        break
                self.click_js(xpathCLick)
        else:
            # if(self.terminalLog == True):print('Dam scrol pana gasim, dar nu click')
            self.scroll(nr_scrolls=initialScroll)
            if self.element_exists(xPath=xpathCLick, silent=True) is True:
                print('Found in first {} scrolls'.format(initialScroll))
            else:
                no_scrols = initialScroll
                while self.element_exists(xPath=xpathCLick, silent=True) is False:
                    self.scroll(nr_scrolls=1)
                    no_scrols += 1
                    if self.element_exists(xPath=xpathCLick, silent=True) is True:
                        break
                    if(self.terminalLog == True):print(no_scrols)
                    if no_scrols > maxScroll:
                        self.log_customStep('Nu se poate selecta o resursa in {}'.format('10'), 'CRITICAL_FAIL')
                        break
                print('Not found in initial {} scrolls, but Found in {} scrolls'.format(initialScroll, no_scrols))

        self.wait_loadingCircle(enable=True)

    def process_browser_log_entry(self, entry):
        response = json.loads(entry['message'])['message']
        return response

    def getHarAsJson(self):
        return self.proxy.har

    def to_posix_path(self,code_path):

        return re.sub("^([A-Za-z])+:",
        lambda match: posixpath.sep + match.group().replace(":", "").lower(),
        pathlib.PureWindowsPath(code_path).as_posix()) if os.name == "posix" else code_path

    def to_posix_full_path(self,code_path):

        return str(self.get_project_root()) + self.to_posix_path(code_path)
         # return self.getTempValue('SRCPath') + self.to_posix_path(code_path)

    def getTestSuit(self, isSanity=False, isFull=False, isCustom=False, testSuiteLocation="defalult"):
        #
        # projectDir = os.path.realpath(__file__)
        # file = None
        # if isCustom == True:
        #     if 'munt' in projectDir.lower():
        #         file = open(
        #             "\\\\vffs\RO\\atp2$\\Integration\\EAI\\Automatizare\\DEX\\TestCases\\Custom\\retention_demoChunk_2.txt")
        #     if 'ciu' in projectDir.lower():
        #         file = open(
        #             "\\\\vffs\RO\\atp2$\\Integration\\EAI\\Automatizare\\DEX\\TestCases\\Custom\\retention_demoChunk_1.txt")
        # if os.name == 'posix':
        #     if isSanity == True:
        #         file = open("constants.txt", "r")
        #
        #     elif isFull:
        #         file = open("constants.txt", "r")
        # else:
        #     if isSanity == True:
        #         if 'munt' in projectDir.lower():
        #             file = open(
        #                 "\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\Sanity\\Chunk_ConfigOferte_1.txt",
        #                 "r")
        #         elif 'fage' in projectDir.lower():
        #             file = open(
        #                 "\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\Sanity\\Chunk_ConfigOferte_2.txt",
        #                 "r")
        #         elif 'ciu' in projectDir.lower():
        #             file = open(
        #                 "\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\Sanity\\Chunk_PachetPromo_2.txt",
        #                 "r")
        #         elif 'dumi' in projectDir.lower():
        #             file = open(
        #                 "\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\Sanity\\Chunk_PachetPromo_1.txt",
        #                 "r")
        #
        #     elif isFull:
        #         if 'ciu' in projectDir.lower():
        #             file = open(
        #                 "\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\Regresion_ModifiedCTDA_TestCases\\Updated_CTDA_Chunk_ConfigOferte_1.txt",
        #                 "r")
        #         elif 'fage' in projectDir.lower():
        #             file = open(
        #                 "\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\Regresion_ModifiedCTDA_TestCases\\Updated_CTDA_Chunk_ConfigOferte_2.txt",
        #                 "r")
        #         elif 'ciu' in projectDir.lower():
        #             file = open(
        #                 "\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\Regresion_ModifiedCTDA_TestCases\\Updated_CTDA_Chunk_PachetPromo_1.txt",
        #                 "r")
        #         elif 'm1unt' in projectDir.lower():
        #             file = open(
        #                 "\\\\vffs\RO\\atp2$\Integration\EAI\Automatizare\DEX\TestCases\Regresion_ModifiedCTDA_TestCases\\Updated_CTDA_Chunk_PachetPromo_2.txt",
        #                 "r")

        if os.name == 'posix':
            file = open("/Code/constants.txt", "r")
            try:
                return ast.literal_eval(file.read())
            except AttributeError:
                print("\tNo suit found for current User")
        else:
            file = open(testSuiteLocation, "r")
            try:
                return ast.literal_eval(file.read())
            except AttributeError:
                print("\tNo suit found for current User")

    def badScenarioEval(self, key, value, recursivityInd, flowType):
        flowType = '_' + flowType

        # if value['searchType'] != 'MSISDN' and '_cbu' not in flowType.lower():
        #     value['searchType'] = 'MSISDN'

        badScenarios = {'IncorectCombination': {},
                        'TBD': {}}

        if 'ACHIZITIE' in value['configType'].upper():
            if '_false' in value['scenarioNameDC'].lower() and '_rate' in value['scenarioName_PC'].lower():
                badScenarios['IncorectCombination'].update({key: value})

                if value['scenarioNameDC'] == 'CR_False_Client_nonMatur':
                    value['scenarioNameDC'] = 'CR_True_Client_nonMatur_ValidateToken_Yes'

                elif value['scenarioNameDC'] == 'CR_False_Client_Matur':
                    value['scenarioNameDC'] = 'CR_True_Client_Matur'

                if value['abNouType'] == 'MFP' and '_NONMATUR' in value['scenarioNameDC']:
                    value['scenarioNameDC'] = 'CR_True_Client_nonMatur_TokenIgnored'

            else:

                if value['abNouType'] == 'MFP' and '_nonmatur' in value['scenarioNameDC'].lower():
                    if '_true' in value['scenarioNameDC'].lower():
                        value['scenarioNameDC'] = 'CR_True_Client_nonMatur_TokenIgnored'
                        badScenarios['IncorectCombination'].update({key: value})

            if len(badScenarios['IncorectCombination']) == 0 and len(badScenarios['TBD']) == 0:
                badScenarios = 'No Bad scenarios had been found for previous suite execution.'
                with open('TestCaseOp/BadScenario{}.txt'.format(recursivityInd), 'w') as opened_file:
                    opened_file.write(str(badScenarios))

            return key, value

        else:
            return key, value

    def requestAPI(self, url, body, isPost=False):
        try:
            if isPost == False:
                rsp = requests.get(url=url, json=body)
                if '500' in rsp:
                    print(f'Request to {url} => {rsp}')
                return (rsp.json())
            else:
                rsp = requests.post(url=url, json=body)
                if '500' in rsp:
                    print(f'Request to {url} => {rsp}')
                return (rsp.json())
        except requests.exceptions.ConnectionError as e:
            print(f'{bcolors.WARN}Connection Error{str(e)}{bcolors.ENDC}')
            return False
        except requests.exceptions.HTTPError as code:
            print(f'{bcolors.WARN}HTTPError {str(code)}{bcolors.ENDC}')
            return False
        except json.decoder.JSONDecodeError as decoder:
            print(f'{bcolors.ERR}Json Decode ERROR:\n {self.format_stacktrace()}\n For URL: {url}{bcolors.ENDC}')
            return {}


    def sendEmailMethod(self, emailContent, email_to='gabriel.munteanu2@vodafone.com'):
        # if(self.terminalLog == True):print(email_to)
        SERVER = "smtp.connex.ro"
        FROM = "dltvoisvfrotestingautomation@vodafone.com"
        TO = email_to.split(',').append(FROM)
        MSG = f"Subject: Test Execution summary\n\n{emailContent}"
        server = smtplib.SMTP(SERVER)
        server.sendmail(FROM, TO, MSG)
        server.quit()
        print(f"Email Send from {__name__}")

    def generateClassInstance(self, classList, flowType):
        flowType = flowType.split("_")[0]
        import bcolors
        # print(classList[0].__name__, 'flt tp', flowType)
        if 'precbu' in flowType.lower():
            pass
        else:
            flowType = '_' + flowType

        for classDefinition in classList:
            if flowType.lower() in classDefinition.__name__.lower():
                print(f'{bcolors.BLUEIC}@Generated {flowType.replace("_"," ").replace("  ", " ").upper()} like object{bcolors.ENDC}')
                print(f'@Obj defined for - conditions => FlowType: ["{flowType.lower()}"], array ClassName: ["{classDefinition.__name__.lower()}"]')
                classInstance = classDefinition()
                return classInstance

            elif flowType.lower() in classDefinition.__name__.lower() and 'config' not in classDefinition.__name__.lower():
                print(f'{bcolors.BLUEIC}@Generated CBU like object{bcolors.ENDC}')
                print('@Obj defined for - conditions', flowType.lower(), classDefinition.__name__.lower())
                classInstance = classDefinition()
                return classInstance

            elif flowType.lower() in classDefinition.__name__.lower() and 'config' not in classDefinition.__name__.lower():
                print(f'{bcolors.BLUEIC}@Generated PRE_CBU like object{bcolors.ENDC}')
                print('@Obj defined for - conditions', flowType.lower(), classDefinition.__name__.lower())
                classInstance = classDefinition()
                return classInstance

    def dir_list_folder(self, head_dir, dir_name):
        """Return a list of the full paths of the subdirectories
        under directory 'head_dir' named 'dir_name'"""
        dirList = []
        for fn in os.listdir(head_dir):
            dirfile = os.path.join(head_dir, fn)
            if os.path.isdir(dirfile):
                if fn.upper() == dir_name.upper():
                    dirList.append(dirfile)
                else:
                    # print "Accessing directory %s" % dirfile
                    dirList += self.dir_list_folder(dirfile, dir_name)
        return dirList

    def find_all_elements(self, xPath, enable_Logging=False, priority=None, silent=True, element_name=None, customTimeout = None):
        #(//div[@class="items-wrap flex"])[5]/*'), cauta toate child elements pentru un path parinte
        customTimeout = self.isGlobalWait(customTimeout)
        xPath = (xPath)
        xPath = xPath + '/*'

        if enable_Logging:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.presence_of_element_located((By.XPATH, xPath)))
                number_of_elements = self.browser.find_elements_by_xpath(xPath)
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='find_all_elements',
                                                            isException=False, browser=self.browser)
                return number_of_elements
            except(NoSuchElementException, StaleElementReferenceException) as e:
                utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='find_all_elements',
                                                            isException=True,
                                                            exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                                str(e), xPath, e), browser=self.browser)
                if silent is None:
                    if(self.terminalLog == True):print(
                        'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath, e))
                else:
                    return False
            except(Exception) as e_timeout:
                if xPath != '//button[@data-automation-id="refresh-error-page"]':

                    utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xPath, priority=priority, action_type='find_all_elements',
                                                                isException=True,
                                                                exeption=f'exceptie {str(e_timeout)} pentru xPath : {xPath}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {customTimeout} sec de wait (TimeoutException)', browser=self.browser)
                    if(self.terminalLog == True):print(
                        'exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                            str(e_timeout), xPath))
                    return False
                else:
                    if(self.terminalLog == True):print('Loading animation validation passed')
        elif enable_Logging is False:
            try:
                WebDriverWait(self.browser, customTimeout).until(ec.presence_of_element_located((By.XPATH, xPath)))
                number_of_elements = self.browser.find_elements_by_xpath(xPath)
                return number_of_elements
            except(NoSuchElementException, StaleElementReferenceException) as e:
                if xPath != '//div[@class="sk-circle-container"]':
                    if(self.terminalLog == True):print(
                        'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xPath,
                                                                                            e))
                return False
            except(Exception) as e_timeout:
                if xPath != '//div[@class="sk-circle-container"]':
                    if(self.terminalLog == True):print(
                        'exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in 3 sec de wait (TimeoutException)'.format(
                            str(e_timeout), xPath))
                return False

    @staticmethod
    def get_project_root() -> Path:
        return Path(__file__).parent.parent

    # returns the number of tabs opened from the current browser session
    def get_numberOfTabs(self):
        try:
            result = self.browser.window_handles
            return len(result)
        except Exception:
            print(self.format_stacktrace())

    def getPegaOfferName(self, inputDictionary):
        configType = self.evalDictionaryValue(inputDictionary, 'configType')
        tipServiciu = self.evalDictionaryValue(inputDictionary, 'tipServiciu')
        abNouType = self.evalDictionaryValue(inputDictionary, 'abNouType')
        scenarioNameDC = self.evalDictionaryValue(inputDictionary, 'scenarioNameDC')
        scenarioName_PC = self.evalDictionaryValue(inputDictionary, 'scenarioName_PC')

        if '_rate' in scenarioName_PC.lower():
            return {'rate': 'RED 9 Better Together rate (discount 52 eur)',
             'discount': 'RED 9 pentru clienti existenti rate (discount 10 eur)'}
        elif 'simo' in scenarioName_PC.lower():
            return {'simonly': 'RED 9 SIM Only',
                    'discount': 'RED 9 Better Together SIM Only'}
        else:
            return {'simonly': 'RED 9 SIM Only',
                    'discount': 'RED 9 Better Together SIM Only'}


        #
        # try:
        #     if tipServiciu == None:
        #         tipServiciu = 'NA'
        #
        #     if 'ACHIZITIE' in configType:
        #         flowType = abNouType
        #     else:
        #         flowType = 'RETENTIE'
        #
        #     if inputDictionary['searchType'] == 'MSISDN':
        #         sub_id = ast.literal_eval(self.retryAPI(API='simpleAPICall', URL=f"http://devops02.connex.ro:8030/getSubId/{inputDictionary['searchValue']}"))
        #     else:
        #         sub_id = {
        #                   "Result": f"{inputDictionary['searchValue']}"
        #                 }
        #
        #     if 'precbu' in inputDictionary['webAppVersion']:
        #         inputInstallments = 36
        #     else:
        #         inputInstallments = 0
        #
        #     body = {
        #         "flowtype": f"{flowType + '_' + inputDictionary['webAppVersion']}",
        #         "scenarioName_PC": f"{scenarioName_PC}",
        #         "isFullOffersDescription": "false",
        #         "inputInstallments": inputInstallments,
        #         "tip_serviciu": f"{tipServiciu}",
        #         "sub_id": int(sub_id['Result']),
        #         'env': inputDictionary['pega_Env']
        #     }
        #     print(body)
        #     url = 'http://devops02.connex.ro:8020/getData/pegaOffers'
        #     rsp = self.requestAPI(url=url, body=body)
        #     if len(rsp) < 2:
        #        self.outlookConnectorMessanger(
        #             f'<pre>For this request =><br>{url}<br><br>Got one or less offers, rsp =><br>{rsp}</pre>')
        #
        #     for key in rsp.keys():
        #         rsp[key] = str(rsp[key]).replace('_24', '').replace('_12', '')
        #     return rsp
        # except Exception as e:
        #     exc_type, exc_obj, exc_tb = sys.exc_info()
        #     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        #     print(f'{bcolors.FAIL}=====================================\n',f'Error name => ["{exc_type}"]\nIn file => ["{fname}"]\nAt line => ["{exc_tb.tb_lineno}"]\nStack trace => ["{e}"]', f'\n-------------------------------------\n{bcolors.ENDC}')
        #     # self.browser.close()

    def validOfferForScenario(self, testName, offersDict, testDefinition, env):

        abortDict = {
       "Test":{
          "Status":"EXECUTION_ABORTED",
          "HOST Name":"LocalHost TBD",
          "Release":"V19 From PyCharm Env",
          "ConnectorLog":"C:\\Users\\MunteanuG\\PycharmProjects\\versionControl\\dex\\Output\\TraceFiles\\None.txt",
          "EXECUTION_TS":"20201127170317",
          "Tests Execution Log":{
             "Test_Aborted":{
                "Status":"EXECUTION_ABORTED",
                "ExecutionLog":{
                    "REASON": f"NO VALID OFFER RETRIVED FROM PEGA @PEGA_ENV : {env}",
                    "PegaOffers": offersDict
                }
             }
          }
       }
    }
        abortDict[testName] = abortDict.pop('Test')
        abortDict[testName]['PegaOffers'] = offersDict

        if len(offersDict) == 0:
            print(f"{bcolors.WARN}{testName}Test Aborted because pega offer missing, empty response body from pega filter API {offersDict}{bcolors.ENDC}")
            return abortDict, True
        elif 'scount' in testDefinition['scenarioName_PC'].lower() and 'discount' not in list(offersDict.keys()):
            print(f"{bcolors.WARN}{testName}Test Aborted because pega offer missing, expected discount offer {offersDict}{bcolors.ENDC}")
            return abortDict, True
        elif 'rate' in testDefinition['scenarioName_PC'].lower() and 'scount' not in testDefinition['scenarioName_PC'].lower() and 'rate' not in list(offersDict.keys()):
            print(f"{bcolors.WARN}{testName}Test Aborted because pega offer missing, expected rate offer {offersDict}{bcolors.ENDC}")
            return abortDict, True
        elif 'simonly' in testDefinition['scenarioName_PC'].lower() and 'scount' not in testDefinition['scenarioName_PC'].lower() and 'simonly' not in list(offersDict.keys()):
            print(f"{bcolors.WARN}{testName}Test Aborted because pega offer missing, expected simonly offer {offersDict}{bcolors.ENDC}")
            return abortDict, True
        else: return {}, False

    def isGlobalWait(self, customTimeout):
        if type(customTimeout) == type(None):
            return self.globalWait
        else:
            return customTimeout

    def titleContains(self, title, priority=None):
        try:
            WebDriverWait(self.browser, 10).until(ec.title_contains(title))
            utilities.VDFloggingObj.priority_handler(element_name=title, xpath=title, priority=priority,
                                                     action_type='Title Exists',
                                                     isException=False, browser=self.browser)
            return True
        except(NoSuchElementException, StaleElementReferenceException) as e:
            utilities.VDFloggingObj.priority_handler(element_name=title, xpath=title, priority=priority,
                                                     action_type='Title Exists',
                                                     isException=True,
                                                     exeption=f'Title not found for input {title} => Exceptie [{e}]')
            return False
        except(Exception) as e_timeout:
            utilities.VDFloggingObj.priority_handler(element_name=title, xpath=title, priority=priority,
                                                     action_type='Title Exists',
                                                     isException=True,
                                                     exeption=f'Title not found for input {title} => Exceptie [{e_timeout}]')
            return False

    def get_Child(self, paramName, full_List=False):
        try:
            child_list = []
            for DictionaryElem in self.inputDictionary:
                for testMethodName, testParamsDict in DictionaryElem.items():
                    for key, val in testParamsDict.items():
                        if key == paramName:
                            child_list.append(val)

            if full_List:
                if child_list == None:
                    # raise IndexError(
                    #     self.VDFloggingObj.terminalLog(paramName=paramName, ParamExternalStatus='NotFound', paramDepthLevel='Got value'))
                    #hacky way to trigger exception todo find a decent way
                    child_list = child_list[0]
                else:
                    self.VDFloggingObj.terminalLog(paramName=paramName, ParamExternalStatus='Success', childList=child_list, paramDepthLevel='All param values', isParamSearch=True)
                    return child_list

            else:
                child_list = child_list[0]
                self.VDFloggingObj.terminalLog(paramName=paramName, ParamExternalStatus='Success', childList=child_list,
                                               paramDepthLevel='First param value', isParamSearch=True)
                return child_list

        except IndexError:
            self.VDFloggingObj.terminalLog(paramName=paramName, ParamExternalStatus='NotFound', paramDepthLevel='Got value', isParamSearch=True)
            return 'NotFound'

    def getData(self, excel_file_path, conditions_Dictionary):
        df = pd.read_excel(excel_file_path, engine='openpyxl')
        query = ''
        count = 0
        for key, val in conditions_Dictionary.items():
            if count == 0:
                query += f'{key} == "{val}"'
                count += 1
            else:
                query += f' and {key} == "{val}"'
        try:
            result = (df.query(query).to_dict(orient='records')[0])
        except Exception:
            result = 'No data found for given conditions'

        try:
            for key in conditions_Dictionary:
                del result[key]
        except Exception:
            pass

        return result

    def element_isClickable(self, xpath, priority=None, enable_Logging=True, externalMethod=None, customTimeout = None, js=True, element_name=None):
        customTimeout = self.isGlobalWait(customTimeout)

        actionType = None
        if externalMethod != None:
            actionType = externalMethod
        else:
            actionType = "element_is_Clickable"

        try:

            WebDriverWait(self.browser, customTimeout).until(ec.element_to_be_clickable((By.XPATH, xpath)))

            # if js:
            #     self.browser.execute_script("arguments[0].click();", self.browser.find_element_by_xpath(xpath))
            # else:
            #     self.browser.find_element_by_xpath(xpath).click()

            utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority,
                                                     action_type=actionType,
                                                     isException=False, browser=self.browser)

            # utilities.VDFloggingObj.terminalLog(testMethod_Name=inspect.currentframe().f_code.co_name, elementName=element_name, priority=priority, exceptionMessage="", actionStatus="PASSED", currentExecLog=self.inputDictionary)
            return True
        except(NoSuchElementException, StaleElementReferenceException) as e:
            if (self.terminalLog == True): print(
                'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath,
                                                                                   e))
            utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority,
                                                     action_type=actionType,
                                                     isException=True,
                                                     exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                         str(e), xpath,
                                                         e), browser=self.browser)
            return False
        except(Exception) as e_timeout:
            utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority,
                                                     action_type=actionType,
                                                     isException=True,
                                                     exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {} sec de wait (TimeoutException)'.format(
                                                         str(e_timeout), xpath, customTimeout), browser=self.browser)

            return False

    def hover(self, xpath, priority=None, enable_Logging=True, externalMethod=None, customTimeout=None,
                            js=True, element_name=None):
        customTimeout = self.isGlobalWait(customTimeout)

        actionType = None
        if externalMethod != None:
            actionType = externalMethod
        else:
            actionType = "hover over"

        try:

            WebDriverWait(self.browser, customTimeout).until(ec.element_to_be_clickable((By.XPATH, xpath)))

            element_to_hover_over = self.browser.find_element_by_xpath(xpath=xpath)
            hover = ActionChains(self.browser).move_to_element(element_to_hover_over)
            hover.perform()

            utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority,
                                                     action_type=actionType,
                                                     isException=False, browser=self.browser)

            # utilities.VDFloggingObj.terminalLog(testMethod_Name=inspect.currentframe().f_code.co_name, elementName=element_name, priority=priority, exceptionMessage="", actionStatus="PASSED", currentExecLog=self.inputDictionary)
            return True
        except(NoSuchElementException, StaleElementReferenceException) as e:
            if (self.terminalLog == True): print(
                'exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(str(e), xpath,
                                                                                   e))
            utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority,
                                                     action_type=actionType,
                                                     isException=True,
                                                     exeption='exceptie {} pentru xPath : {}\nException Stack Trace : {}'.format(
                                                         str(e), xpath,
                                                         e), browser=self.browser)
            return False
        except(Exception) as e_timeout:
            utilities.VDFloggingObj.priority_handler(element_name=element_name, xpath=xpath, priority=priority,
                                                     action_type=actionType,
                                                     isException=True,
                                                     exeption='exceptie {} pentru xPath : {}\nException Stack Trace : Elementul nu a fost vizibil pe pagina in {} sec de wait (TimeoutException)'.format(
                                                         str(e_timeout), xpath, customTimeout), browser=self.browser)

            return False
 

if __name__ == '__main__':
    test_definition = [{"MTV175-F817 - refreshButton - Click on the Refresh button": [
        {"page_Login": {
            "pageLogin_MainScenario": "Success",
            "pageLogin_DealerCode": "singleDealerCode",
            "pageLogin_Prerequisite": "TLS + FRUT + Agent",
        }
        },
        {"page_Search": {
            "pageSearch_MainScenario": "myOrders",
        }
        },
        {"page_AgentOrders": {
            "pageAgentOrders_OrderID": "234567",
            "pageAgentOrders_OrderStatus": "In progress",
            "pageAgentOrders_orderFlowDesc": "refresh"
        }
        }
    ]
    }]

    utils = utilities(inputDictionary=test_definition)
    conditions = {"pageLogin_mainScenario": "Succesful_login", "UserName": "John"}
    test_data = utils.getData(conditions_Dictionary=conditions, excel_file_path="Book1.xlsx")
    print(test_data)

